import graphene

import cafe.schema
import graphql_jwt
import graphene
from rx import Observable



class Mutation(cafe.schema.Mutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Query(cafe.schema.Query, graphene.ObjectType):
    pass


class Subscription(cafe.schema.Subscription, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation, subscription=Subscription)