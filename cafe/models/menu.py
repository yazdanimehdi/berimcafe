from django.db import models


class IngredientsCategory(models.Model):
    name = models.TextField()
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)


class Ingredients(models.Model):
    name = models.TextField()
    price = models.IntegerField()
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(to='IngredientsCategory', on_delete=models.SET_NULL, null=True, blank=True)
    scale = models.TextField()
    related_place = models.ForeignKey(to='Place', on_delete=models.SET_NULL, null=True, blank=True)


class FoodIngredients(models.Model):
    food = models.ForeignKey(to='Food', on_delete=models.CASCADE)
    ingredient = models.ForeignKey(to='Ingredients', on_delete=models.CASCADE)
    amount = models.FloatField()


class FoodCategory(models.Model):
    name = models.TextField()
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)


class Food(models.Model):
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    price = models.FloatField()
    category = models.ForeignKey(to='FoodCategory', on_delete=models.SET_NULL, null=True, blank=True)
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)
    discount_percent = models.IntegerField(null=True, blank=True)
    discount = models.IntegerField(null=True, blank=True)
    status = models.BooleanField(default=False)

    class ProductionPlace(models.TextChoices):
        KITCHEN = 'KI'
        BAR = 'BA'

    related_production_place = models.CharField(max_length=2,
                                                choices=ProductionPlace.choices,)


class Order(models.Model):
    food = models.ForeignKey(to='Food', on_delete=models.SET_NULL, null=True, blank=True)
    invoice = models.ForeignKey(to='Invoice', on_delete=models.CASCADE)
    date_and_time_Submitted = models.DateTimeField(auto_now=True)
    date_and_time_delivered = models.DateTimeField(null=True, blank=True)
    discount_percent = models.IntegerField(null=True, blank=True)
    discount = models.IntegerField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    amount = models.IntegerField()

    class Status(models.TextChoices):
        PENDING = 'PE'
        DONE = 'DO'

    status = models.CharField(max_length=2, choices=Status.choices, default=Status.PENDING)


class PaymentMethod(models.Model):
    name = models.TextField()
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)


class Invoice(models.Model):
    customer = models.ForeignKey(to='Customer', on_delete=models.SET_NULL, null=True, blank=True)
    waiter = models.ForeignKey(to='Personnel', on_delete=models.SET_NULL, null=True, blank=True)
    table = models.ForeignKey(to='Table', on_delete=models.SET_NULL, null=True, blank=True)
    related_place = models.ForeignKey(to='Place', on_delete=models.SET_NULL, null=True, blank=True)
    number = models.IntegerField()
    date_and_time = models.DateTimeField(auto_now=True)
    discount_percent = models.IntegerField(null=True, blank=True)
    discount = models.IntegerField(null=True, blank=True)

    class Status(models.TextChoices):
        PENDING = 'PE'
        DONE = 'DO'

    status = models.CharField(max_length=2, choices=Status.choices, default=Status.PENDING)

    payment_method = models.ForeignKey(to='PaymentMethod', on_delete=models.SET_NULL, null=True, blank=True)


class InvoicePersonnel(models.Model):
    customer = models.ForeignKey(to='Personnel', on_delete=models.SET_NULL, null=True, blank=True)
    date_and_time = models.DateTimeField(auto_now=True)
    related_place = models.ForeignKey(to='Place', on_delete=models.SET_NULL, null=True, blank=True)


