from django.db import models


class Place(models.Model):
    name = models.TextField()
    phone1 = models.IntegerField()
    phone2 = models.IntegerField(null=True, blank=True)
    phone3 = models.IntegerField(null=True, blank=True)
    phone4 = models.IntegerField(null=True, blank=True)
    emergency_phone = models.IntegerField()
    email = models.EmailField()
    personnel_add = models.IntegerField(default=1)
    website = models.TextField(null=True, blank=True)
    instagram = models.TextField(null=True, blank=True)
    telegram = models.TextField(null=True, blank=True)
    foursquare = models.TextField(null=True, blank=True)
    tweeter = models.TextField(null=True, blank=True)
    opening_hours = models.CharField(
        verbose_name='restaurant_opening_hours',
        max_length=50,
    )

    LOW = 'LOW'
    MEDIUM = 'MEDIUM'
    HIGH = 'HIGH'
    PRICE_LEVEL_CHOICES = (
        (LOW, '$'),
        (MEDIUM, '$$'),
        (HIGH, '$$$'),
    )

    price_level = models.CharField(
        verbose_name='restaurant_price_level',
        max_length=6,
        choices=PRICE_LEVEL_CHOICES,
    )

    image = models.ImageField(
        verbose_name='restaurant_image',
        blank=True,
        null=True
    )



    class Smoke(models.TextChoices):
        SMOKEFREE = 'SF'
        SMOKE = 'S'

    smoking_status = models.CharField(max_length=2, choices=Smoke.choices, default=Smoke.SMOKEFREE)

    class PlaceType(models.TextChoices):
        CAFE = 'CA'
        RESTAURANT = 'RE'
        TABAKHI = 'TA'
        JIGARAKI = 'JI'
        BAKERY = 'BA'

    place_type = models.CharField(max_length=2, choices=PlaceType.choices, default=PlaceType.CAFE)

    class CafeType(models.TextChoices):
        CAFERESTURANT = 'CR'
        CAFEFASTFOOD = 'CFF'
        CAFESHOP = 'CS'
        CAFEBAKERY = 'CB'
        TAKEAWAY = 'TA'
        CAFEGHELYOON = 'CG'

    cafe_type = models.CharField(max_length=3, choices=CafeType.choices, null=True, blank=True)

    class ResturantType(models.TextChoices):
        IRANIAN = 'IR'
        INTERNATIONAL = 'IN'

    restaurant_type = models.CharField(max_length=2, choices=ResturantType.choices, null=True, blank=True)


class Saloon(models.Model):
    name = models.TextField()
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)


class Table(models.Model):
    table_name = models.TextField()
    related_saloon = models.ForeignKey(to='Saloon', on_delete=models.CASCADE)
    chair_number = models.IntegerField()
    occupied = models.BooleanField(default=False)






