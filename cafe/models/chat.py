from django.db import models
from cafe.models import Personnel, Place


class ChatSession (models.Model):
    name = models.TextField(null=True, blank=True)
    users = models.ManyToManyField(Personnel)
    date_added = models.DateTimeField(auto_created=True, auto_now=True)
    related_place = models.ForeignKey(to=Place, on_delete=models.CASCADE, null=True, blank=True)


class ChatMessages(models.Model):
    chat = models.ForeignKey(to=ChatSession, on_delete=models.CASCADE)
    from_user = models.ForeignKey(to=Personnel, on_delete=models.SET_NULL, null=True)
    date_time = models.DateTimeField(auto_now=True, auto_created=True)
    reply_message = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    file = models.FileField(null=True, blank=True)
    read = models.BooleanField(default=False)


