from django.db import models
from django.contrib.auth.models import AbstractUser


class Customer(models.Model):
    related_place = models.ForeignKey(to='Place', on_delete=models.SET_NULL, null=True, blank=True)
    name = models.TextField()
    phone1 = models.TextField(null=True, blank=True)
    phone2 = models.TextField(null=True, blank=True)
    email1 = models.EmailField(null=True, blank=True)
    email2 = models.EmailField(null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    extra_info = models.TextField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)

