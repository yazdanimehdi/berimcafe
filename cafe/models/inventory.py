from django.db import models


class InventoryIngredients(models.Model):
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)
    ingredient = models.ForeignKey(to='Ingredients', on_delete=models.CASCADE)
    amount = models.FloatField(default=0.0)


class InventoryEntry(models.Model):
    description = models.TextField(null=True, blank=True)
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)
    amount = models.FloatField()
    date_time = models.DateTimeField()
    ingredient = models.ForeignKey(to='Ingredients', on_delete=models.SET_NULL, null=True, blank=True)
    cost = models.FloatField()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        inventory_ingredient = InventoryIngredients.objects.get_or_create(ingredient=self.ingredient,
                                                                          related_place=self.related_place)
        inventory_ingredient.amount += self.amount
        inventory_ingredient.save()
        self.ingredient.price = self.amount / self.cost
        self.ingredient.save()
        super(InventoryEntry, self).save(force_insert=False, force_update=False, using=None,
                                         update_fields=None)

    def delete(self, using=None, keep_parents=False):
        inventory_ingredient = InventoryIngredients.objects.get(ingredient=self.ingredient,
                                                                related_place=self.related_place)
        inventory_ingredient.amount -= self.amount
        inventory_ingredient.save()
        super(InventoryEntry, self).delete()


class InventoryOut(models.Model):
    description = models.TextField(null=True, blank=True)
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)
    amount = models.FloatField()
    date_time = models.DateTimeField()
    ingredient = models.ForeignKey(to='Ingredients', on_delete=models.SET_NULL, null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        inventory_ingredient = InventoryIngredients.objects.get_or_create(ingredient=self.ingredient,
                                                                          related_place=self.related_place)
        inventory_ingredient.amount -= self.amount
        inventory_ingredient.save()
        super(InventoryOut, self).save(force_insert=False, force_update=False, using=None,
                                       update_fields=None)

    def delete(self, using=None, keep_parents=False):
        inventory_ingredient = InventoryIngredients.objects.get(ingredient=self.ingredient,
                                                                related_place=self.related_place)
        inventory_ingredient.amount += self.amount
        inventory_ingredient.save()
        super(InventoryOut, self).delete()
