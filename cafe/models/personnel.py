from django.contrib.auth.models import AbstractUser
from django.db import models

PERMISSION_CHOICES = [
    ('CustomerAll', '(0, 0)'),
    ('CustomerName', '(0, 1)'),
    ('CustomerAdd', '(0, 2)'),
    ('CustomerDelete', '(0, 3)'),
    ('CustomerEdit', '(0, 4)'),
    ('CustomerReadOnly', '(0, 5)'),
    ('OrderAll', '(1, 0)'),
    ('OrderAdd', '(1, 1)'),
    ('OrderCancel', '(1, 2)'),
    ('OrderEdit', '(1, 3)'),
    ('OrderReadOnly', '(1, 4)'),
    ('InvoiceAll', '(2, 0)'),
    ('InvoiceAdd', '(2, 1)'),
    ('InvoiceCancel', '(2, 2)'),
    ('InvoiceEdit', '(2, 3)'),
    ('InvoiceReadOnly', '(2, 4)'),
    ('PersonnelAll', '(3, 0)'),
    ('PersonnelAdd', '(3, 1)'),
    ('PersonnelDelete', '(3, 2)'),
    ('PersonnelEdit', '(3, 3)'),
    ('PersonnelReadOnly', '(3, 4)'),
    ('ShiftAll', '(4, 0)'),
    ('ShiftAdd', '(4, 1)'),
    ('ShiftDelete', '(4, 2)'),
    ('ShiftEdit', '(4, 3)'),
    ('ShiftReadOnly', '(4, 4)'),
    ('ShiftPersonnelAll', '(5, 0)'),
    ('ShiftPersonnelAdd', '(5, 1)'),
    ('ShiftPersonnelDelete', '(5, 2)'),
    ('ShiftPersonnelEdit', '(5, 3)'),
    ('ShiftPersonnelReadOnly', '(5, 4)'),
    ('RoleAll', '(6, 0)'),
    ('RoleReadOnly', '(6, 1)'),
    ('InventoryAll', '(7, 0)'),
    ('InventoryAdd', '(7, 1)'),
    ('InventoryDelete', '(7, 2)'),
    ('InventoryEdit', '(7, 3)'),
    ('InventoryReadOnly', '(7, 4)'),
    ('FoodAll', '(8, 0)'),
    ('FoodAdd', '(8, 1)'),
    ('FoodDelete', '(8, 2)'),
    ('FoodEdit', '(8, 3)'),
    ('FoodReadOnly', '(8, 4)'),
    ('IngredientsAll', '(9, 0)'),
    ('IngredientsAdd', '(9, 1)'),
    ('IngredientsDelete', '(9, 2)'),
    ('IngredientsEdit', '(9, 3)'),
    ('IngredientsReadOnly', '(9, 4)'),
    ('PlaceAll', '(10, 0)'),
    ('PlaceAdd', '(10, 1)'),
    ('PlaceDelete', '(10, 2)'),
    ('PlaceEdit', '(10, 3)'),
    ('PlaceReadOnly', '(10, 4)'),
    ('SaloonAll', '(11, 0)'),
    ('SaloonAdd', '(11, 1)'),
    ('SaloonDelete', '(11, 2)'),
    ('SaloonEdit', '(11, 3)'),
    ('SaloonReadOnly', '(11, 4)'),
    ('TableAll', '(12, 0)'),
    ('TableAdd', '(12, 1)'),
    ('TableDelete', '(12, 2)'),
    ('TableEdit', '(12, 3)'),
    ('TableReadOnly', '(12, 4)'),
    ('ChatAll', '(13, 0)')

]


class Permissions(models.Model):
    permission = models.CharField(max_length=30, choices=PERMISSION_CHOICES)


class PermissionsRole(models.Model):
    role = models.ForeignKey(to='Role', on_delete=models.CASCADE)
    permission = models.ForeignKey(to='Permissions', on_delete=models.CASCADE)


class Role(models.Model):
    name = models.TextField()

    class Related(models.Choices):
        SALOON = 'SA'
        BAR = 'BA'
        KITCHEN = 'KI'

    role_related = models.CharField(max_length=2, choices=Related.choices, null=True, blank=True)
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)


class RolePersonnel(models.Model):
    personnel = models.ForeignKey(to='Personnel', on_delete=models.CASCADE)
    role = models.ForeignKey(to='Role', on_delete=models.CASCADE)


class Personnel(AbstractUser):
    phone = models.CharField(max_length=11,null=True, blank=True)
    photo = models.ImageField(null=True, blank=True)
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE, null=True, blank=True)

    class Gender(models.Choices):
        MALE = 'M'
        FEMALE = 'F'

    gender = models.CharField(max_length=1, choices=Gender.choices, default=Gender.MALE)


class PersonnelShift(models.Model):
    personnel = models.ForeignKey(to='Personnel', on_delete=models.CASCADE)
    shift = models.ForeignKey(to='Shift', on_delete=models.CASCADE)


class Shift(models.Model):

    class Day(models.TextChoices):
        SATURDAY = 'SAT'
        SUNDAY = 'SUN'
        MONDAY = 'MON'
        TUESDAY = 'TUS'
        WEDNESDAY = 'WED'
        THURSDAY = 'THU'
        FRIDAY = 'FRI'

    day = models.CharField(max_length=3, choices=Day.choices)
    start_time = models.TimeField()
    end_time = models.TimeField()
    related_place = models.ForeignKey(to='Place', on_delete=models.CASCADE)
