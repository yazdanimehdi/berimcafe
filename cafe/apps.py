from django.apps import AppConfig


class CafeConfig(AppConfig):
    name = 'cafe'

    def ready(self):
        import cafe.signals



