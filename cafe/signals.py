from django.db.models.signals import post_save, post_delete
from graphene_subscriptions.signals import post_save_subscription, post_delete_subscription

from cafe.models import ChatMessages

post_save.connect(post_save_subscription, sender=ChatMessages, dispatch_uid="chat_messages_post_save")
post_delete.connect(post_delete_subscription, sender=ChatMessages, dispatch_uid="chat_messages_post_delete")