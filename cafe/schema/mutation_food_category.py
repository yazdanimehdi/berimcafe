import graphene
from cafe.models import FoodCategory, RolePersonnel, Permissions
from .types import FoodCategoryType
from graphql_jwt.decorators import login_required


class FoodCategoryAddMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()

    food_category = graphene.Field(FoodCategoryType)

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodAdd' in permissions_list:
            food_category_inst = FoodCategory()
            food_category_inst.name = kwargs.get('name')
            food_category_inst.related_place = user.related_place
            food_category_inst.save()
            return FoodCategoryAddMutation(food_category=food_category_inst)


class FoodCategoryEditMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()
        food_category_id = graphene.ID()

    food_category = graphene.Field(FoodCategoryType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, food_category_id,  **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodEdit' in permissions_list:
            try:
                food_category_inst = FoodCategory.objects.get(id=food_category_id)
                if kwargs.get('name'):
                    food_category_inst.name = kwargs.get('name')
                food_category_inst.save()
                return FoodCategoryEditMutation(food_category=food_category_inst, message='', status=True)
            except FoodCategory.DoesNotExist:
                return FoodCategoryEditMutation(food_category=None, message='Food Category Does Not Exist', status=False)


class FoodCategoryDeleteMutation(graphene.Mutation):

    class Arguments:
        food_category_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, food_category_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodDelete' in permissions_list:
            try:
                food_category_inst = FoodCategory.objects.get(id=food_category_id)
                food_category_inst.delete()
                return FoodCategoryDeleteMutation(status=True)
            except FoodCategory.DoesNotExist:
                return FoodCategoryDeleteMutation(status=False)

