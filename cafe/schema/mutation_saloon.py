import graphene
from cafe.models import Saloon, RolePersonnel, Permissions
from .types import SaloonType
from graphql_jwt.decorators import login_required


class SaloonAddMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()

    saloon = graphene.Field(SaloonType)

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'SaloonAll' in permissions_list or 'SaloonAdd' in permissions_list:
            saloon_inst = Saloon()
            saloon_inst.name = kwargs.get('name')
            saloon_inst.related_place = user.related_place
            saloon_inst.save()
            return SaloonAddMutation(saloon_inst)


class SaloonEditMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()
        saloon_id = graphene.ID()

    saloon = graphene.Field(SaloonType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, saloon_id,  **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'SaloonAll' in permissions_list or 'SaloonEdit' in permissions_list:
            try:
                saloon_inst = Saloon.objects.get(id=saloon_id)
                if kwargs.get('name'):
                    saloon_inst.name = kwargs.get('name')
                saloon_inst.save()
                return SaloonEditMutation(saloon=saloon_inst, message='', status=True)
            except Saloon.DoesNotExist:
                return SaloonEditMutation(saloon=None, message='Saloon Does Not Exist', status=False)


class SaloonDeleteMutation(graphene.Mutation):

    class Arguments:
        saloon_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, saloon_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'SaloonAll' in permissions_list or 'SaloonDelete' in permissions_list:
            try:
                saloon_inst = Saloon.objects.get(id=saloon_id)
                saloon_inst.delete()
                return SaloonDeleteMutation(status=True)
            except Saloon.DoesNotExist:
                return SaloonDeleteMutation(status=False)

