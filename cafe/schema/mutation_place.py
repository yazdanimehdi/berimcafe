import graphene
from cafe.models import Place, RolePersonnel, Permissions
from .types import PlaceType
from graphene_file_upload.scalars import Upload
from django.core.files import File
from graphql_jwt.decorators import login_required


class PlaceAddMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        phone1 = graphene.Int()
        phone2 = graphene.Int()
        phone3 = graphene.Int()
        phone4 = graphene.Int()
        emergency_phone = graphene.Int()
        email = graphene.String()
        website = graphene.String()
        instagram = graphene.String()
        telegram = graphene.String()
        foursquare = graphene.String()
        tweeter = graphene.String()
        opening_hours = graphene.String()
        price_level = graphene.String()
        image = Upload(required=False)
        smoking_status = graphene.String()
        place_type = graphene.String()
        cafe_type = graphene.String()
        restaurant_type = graphene.String()

    place = graphene.Field(PlaceType)

    @login_required
    def mutate(self, info, image, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PlaceAll' in permissions_list or 'PlaceAdd' in permissions_list:
            place_inst = Place()
            place_inst.name = kwargs.get('name')
            place_inst.phone1 = kwargs.get('phone1')
            place_inst.phone2 = kwargs.get('phone2')
            place_inst.phone3 = kwargs.get('phone3')
            place_inst.phone4 = kwargs.get('phone4')
            place_inst.cafe_type = kwargs.get('cafe_type')
            place_inst.email = kwargs.get('email')
            place_inst.emergency_phone = kwargs.get('emergency_phone')
            place_inst.smoking_status = kwargs.get('smoking_status')
            place_inst.foursquare = kwargs.get('foursquare')
            place_inst.instagram = kwargs.get('instagram')
            place_inst.tweeter = kwargs.get('tweeter')
            place_inst.telegram = kwargs.get('telegram')
            place_inst.price_level = kwargs.get('price_level')
            place_inst.image = File(image)
            place_inst.opening_hours = kwargs.get('opening_hours')
            place_inst.place_type = kwargs.get('place_type')
            place_inst.restaurant_type = kwargs.get('restaurant_type')
            place_inst.website = kwargs.get('website')
            place_inst.save()
            return PlaceAddMutation(place=place_inst)


class PlaceEditMutation(graphene.Mutation):

    class Arguments:
        place_id = graphene.ID(required=True)
        name = graphene.String(required=True)
        phone1 = graphene.Int()
        phone2 = graphene.Int()
        phone3 = graphene.Int()
        phone4 = graphene.Int()
        emergency_phone = graphene.Int()
        email = graphene.String()
        website = graphene.String()
        instagram = graphene.String()
        telegram = graphene.String()
        foursquare = graphene.String()
        tweeter = graphene.String()
        opening_hours = graphene.String()
        price_level = graphene.String()
        image = Upload(required=False)
        smoking_status = graphene.String()
        place_type = graphene.String()
        cafe_type = graphene.String()
        restaurant_type = graphene.String()

    place = graphene.Field(PlaceType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, place_id,  image, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PlaceAll' in permissions_list or 'PlaceEdit' in permissions_list:
            try:
                place_inst = Place.objects.get(id=place_id)
                if kwargs.get('name'):
                    place_inst.name = kwargs.get('name')
                if kwargs.get('phone1'):
                    place_inst.phone1 = kwargs.get('phone1')
                if kwargs.get('phone2'):
                    place_inst.phone2 = kwargs.get('phone2')
                if kwargs.get('phone3'):
                    place_inst.phone3 = kwargs.get('phone3')
                if kwargs.get('phone4'):
                    place_inst.phone4 = kwargs.get('phone4')
                if kwargs.get('cafe_type'):
                    place_inst.cafe_type = kwargs.get('cafe_type')
                if kwargs.get('email'):
                    place_inst.email = kwargs.get('email')
                if kwargs.get('emergency_phone'):
                    place_inst.emergency_phone = kwargs.get('emergency_phone')
                if kwargs.get('smoking_status'):
                    place_inst.smoking_status = kwargs.get('smoking_status')
                if kwargs.get('foursquare'):
                    place_inst.foursquare = kwargs.get('foursquare')
                if kwargs.get('instagram'):
                    place_inst.instagram = kwargs.get('instagram')
                if kwargs.get('tweeter'):
                    place_inst.tweeter = kwargs.get('tweeter')
                if kwargs.get('telegram'):
                    place_inst.telegram = kwargs.get('telegram')
                if kwargs.get('price_level'):
                    place_inst.price_level = kwargs.get('price_level')
                if image:
                    place_inst.image = File(image)
                if kwargs.get('opening_hours'):
                    place_inst.opening_hours = kwargs.get('opening_hours')
                if kwargs.get('place_type'):
                    place_inst.place_type = kwargs.get('place_type')
                if kwargs.get('restaurant_type'):
                    place_inst.restaurant_type = kwargs.get('restaurant_type')
                if kwargs.get('website'):
                    place_inst.website = kwargs.get('website')
                place_inst.save()
                return PlaceEditMutation(place=place_inst, status=True, message='')
            except Place.DoesNotExist:
                return PlaceEditMutation(place=None, status=False, message='Place Does Not Exist')


class PlaceDeleteMutation(graphene.Mutation):

    class Arguments:
        place_id = graphene.ID(required=True)

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, place_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PlaceAll' in permissions_list or 'PlaceDelete' in permissions_list:
            try:
                place = Place.objects.get(id=place_id)
                place.delete()
                return PlaceDeleteMutation(True)
            except Place.DoesNotExist:
                return PlaceDeleteMutation(False)
