import graphene
from cafe.models import IngredientsCategory, RolePersonnel, Permissions
from .types import IngredientsCategoryType
from graphql_jwt.decorators import login_required


class IngredientsCategoryAddMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()

    ingredient_category = graphene.Field(IngredientsCategoryType)

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'IngredientsAll' in permissions_list or 'IngredientsAdd' in permissions_list:
            ingredients_category_inst = IngredientsCategory()
            ingredients_category_inst.name = kwargs.get('name')
            ingredients_category_inst.related_place = user.related_place
            ingredients_category_inst.save()
            return IngredientsCategoryAddMutation(ingredient_category=ingredients_category_inst)


class IngredientsCategoryEditMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()
        ingredients_category_id = graphene.ID()

    ingredients_category = graphene.Field(IngredientsCategoryType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, ingredients_category_id,  **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'IngredientsAll' in permissions_list or 'IngredientsAdd' in permissions_list:
            try:
                ingredients_category_inst = IngredientsCategory.objects.get(id=ingredients_category_id)
                if kwargs.get('name'):
                    ingredients_category_inst.name = kwargs.get('name')
                ingredients_category_inst.save()
                return IngredientsCategoryEditMutation(ingredients_category=ingredients_category_inst, message='',
                                                       status=True)
            except IngredientsCategory.DoesNotExist:
                return IngredientsCategoryEditMutation(ingredients_category=None, message='Food Category Does Not Exist', status=False)


class IngredientsCategoryDeleteMutation(graphene.Mutation):

    class Arguments:
        ingredients_category_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, ingredients_category_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'IngredientsAll' in permissions_list or 'IngredientsDelete' in permissions_list:
            try:
                ingredients_category_inst = IngredientsCategory.objects.get(id=ingredients_category_id)
                ingredients_category_inst.delete()
                return IngredientsCategoryDeleteMutation(status=True)
            except IngredientsCategory.DoesNotExist:
                return IngredientsCategoryDeleteMutation(status=False)

