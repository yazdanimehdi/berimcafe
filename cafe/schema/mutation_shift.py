import graphene
from cafe.models import Shift, RolePersonnel, Permissions
from .types import ShiftType
from graphql_jwt.decorators import login_required


class ShiftAddMutation(graphene.Mutation):

    class Arguments:
        day = graphene.String()
        start_time = graphene.Time()
        end_time = graphene.Time()

    shift = graphene.Field(ShiftType)

    @login_required
    def mutate(self, info, day, start_time, end_time):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'ShiftAll' in permissions_list or 'ShiftAdd' in permissions_list:
            shift_inst = Shift()
            shift_inst.day = day
            shift_inst.start_time = start_time
            shift_inst.end_time = end_time
            shift_inst.save()
            return ShiftAddMutation(shift=shift_inst)


class ShiftEditMutation(graphene.Mutation):

    class Arguments:
        shift_id = graphene.ID()
        day = graphene.String()
        start_time = graphene.Time()
        end_time = graphene.Time()

    shift = graphene.Field(ShiftType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, shift_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'ShiftAll' in permissions_list or 'ShiftEdit' in permissions_list:
            try:
                shift_inst = Shift.objects.get(id=shift_id)
                if kwargs.get('day'):
                    shift_inst.day = kwargs.get('day')
                if kwargs.get('start_time'):
                    shift_inst.start_time = kwargs.get('start_time')
                if kwargs.get('end_time'):
                    shift_inst.end_time = kwargs.get('end_time')

                shift_inst.save()
                return ShiftEditMutation(shift=shift_inst, message='', status=True)
            except Shift.DoesNotExist:
                return ShiftEditMutation(shift=None, message='Shift Does Not Exist', status=False)


class ShiftDeleteMutation(graphene.Mutation):

    class Arguments:
        shift_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, shift_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'ShiftAll' in permissions_list or 'ShiftDelete' in permissions_list:
            try:
                shift_inst = Shift.objects.get(id=shift_id)
                shift_inst.delete()
                return ShiftDeleteMutation(status=True)

            except Shift.DoesNotExist:
                return ShiftDeleteMutation(status=False)