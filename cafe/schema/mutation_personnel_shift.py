import graphene
from cafe.models import Shift, PersonnelShift, Personnel, RolePersonnel, Permissions
from .types import PersonnelShiftType
from graphql_jwt.decorators import login_required


class PersonnelShiftAddMutation(graphene.Mutation):

    class Arguments:
        personnel_id = graphene.ID()
        shift_id = graphene.ID()

    personnel_shift = graphene.Field(PersonnelShiftType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, personnel_id, shift_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'ShiftPersonnelAll' in permissions_list or 'ShiftPersonnelAdd' in permissions_list:
            personnel_shift_inst = PersonnelShift()
            try:
                personnel_shift_inst.personnel = Personnel.objects.get(id=personnel_id)
            except Personnel.DoesNotExist:
                return PersonnelShiftAddMutation(personnel_shift=None, message='Personnel Does Not Exist', status=False)

            try:
                personnel_shift_inst.shift = Shift.objects.get(id=shift_id)
            except Shift.DoesNotExist:
                return PersonnelShiftAddMutation(personnel_shift=None, message='Shift Does Not Exist', status=False)

            personnel_shift_inst.save()
            return PersonnelShiftAddMutation(personnel_shift=personnel_shift_inst, message='', status=True)


class PersonnelShiftEditMutation(graphene.Mutation):

    class Arguments:
        personnel_shift_id = graphene.ID()
        shift_id = graphene.ID()
        personnel_id = graphene.ID()

    personnel_shift = graphene.Field(PersonnelShiftType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, personnel_shift_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'ShiftPersonnelAll' in permissions_list or 'ShiftPersonnelEdit' in permissions_list:
            try:
                personnel_shift_inst = PersonnelShift.objects.get(id=personnel_shift_id)
                if kwargs.get('personnel_id'):
                    try:
                        personnel_shift_inst.personnel = Personnel.objects.get(id=kwargs.get('personnel_id'))
                    except Personnel.DoesNotExist:
                        return PersonnelShiftEditMutation(personnel_shift=None, message='Personnel Does Not Exist',
                                                          status=False)
                if kwargs.get('shift_id'):
                    try:
                        personnel_shift_inst.shift = Shift.objects.get(id=kwargs.get('shift_id'))
                    except Shift.DoesNotExist:
                        return PersonnelShiftEditMutation(personnel_shift=None, message='Shift Does Not Exist',
                                                          status=False)

                personnel_shift_inst.save()
                return PersonnelShiftEditMutation(personnel_shift=personnel_shift_inst, message='', status=True)
            except PersonnelShift.DoesNotExist:
                return PersonnelShiftEditMutation(personnel_shift=None, message='Personnel Shift Does Not Exist',
                                                  status=False)


class PersonnelShiftDeleteMutation(graphene.Mutation):

    class Arguments:
        personnel_shift_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, personnel_shift_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'ShiftPersonnelAll' in permissions_list or 'ShiftPersonnelDelete' in permissions_list:
            try:
                personnel_shift_inst = PersonnelShift.objects.get(id=personnel_shift_id)
                personnel_shift_inst.delete()
                return PersonnelShiftDeleteMutation(status=True)

            except PersonnelShift.DoesNotExist:
                return PersonnelShiftDeleteMutation(status=False)