import graphene
from cafe.models import InventoryEntry, RolePersonnel, Permissions, Ingredients
from .types import InventoryEntryType
from graphql_jwt.decorators import login_required


class InventoryEntryAddMutation(graphene.Mutation):
    class Arguments:
        ingredient_id = graphene.ID()
        description = graphene.String()
        cost = graphene.Float()
        amount = graphene.Float()
        date_time = graphene.DateTime()

    inventory_entry = graphene.Field(InventoryEntryType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InventoryAll' in permissions_list or 'InventoryAdd' in permissions_list:
            inventory_entry_inst = InventoryEntry()
            inventory_entry_inst.related_place = user.related_place
            try:
                inventory_entry_inst.ingredient = Ingredients.objects.get(id=int(kwargs.get('ingredient_id')))
            except Ingredients.DoesNotExist:
                return InventoryEntryAddMutation(inventory_entry=None, message='Ingredient Does Not Exist', status=False)

            if kwargs.get('description'):
                inventory_entry_inst.description = kwargs.get('description')

            inventory_entry_inst.cost = kwargs.get('cost')
            inventory_entry_inst.amount = kwargs.get('amount')
            inventory_entry_inst.date_time = kwargs.get('date_time')
            inventory_entry_inst.save()
            return InventoryEntryAddMutation(inventory_entry=inventory_entry_inst, message='', status=True)


class InventoryEntryDeleteMutation(graphene.Mutation):
    class Arguments:
        inventory_entry_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, inventory_entry_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InventoryAll' in permissions_list or 'InventoryDelete' in permissions_list:
            try:
                InventoryEntry.objects.get(id=inventory_entry_id).delete()
                return InventoryEntryDeleteMutation(status=True)
            except InventoryEntry.DoesNotExist:
                return InventoryEntryDeleteMutation(status=False)
