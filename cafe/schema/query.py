import graphene
from .types import *
from graphql_jwt.decorators import login_required


class Query:
    all_customers = graphene.List(CustomerType)
    all_customers_name = graphene.List(CustomerNameType)
    all_places = graphene.List(PlaceType)
    all_saloons = graphene.List(SaloonType)
    all_tables = graphene.List(TableType)
    all_inventory_ingredients = graphene.List(InventoryIngredientsType)
    all_invoice = graphene.List(InvoiceType)
    all_food = graphene.List(FoodType)
    all_food_category = graphene.List(FoodCategoryType)
    all_ingredients = graphene.List(IngredientsType)
    all_ingredients_category = graphene.List(IngredientsCategoryType)
    all_food_ingredients = graphene.List(FoodIngredientsType)
    all_order = graphene.List(OrderType)
    all_shifts = graphene.List(ShiftType)
    all_personnel_shift = graphene.List(PersonnelShiftType)
    all_personnel = graphene.List(PersonnelType)
    me = graphene.Field(PersonnelType)
    all_food_without_category = graphene.List(FoodType)
    all_ingredients_without_category = graphene.List(IngredientsType)
    all_inventory_entry = graphene.List(InventoryEntryType, ingredient_id=graphene.ID(required=False),
                                        time_start=graphene.DateTime(required=False),
                                        time_end=graphene.DateTime(required=False))
    all_inventory_out = graphene.List(InventoryOutType, ingredient_id=graphene.ID(required=False),
                                      time=graphene.DateTime(required=False),
                                      time_end=graphene.DateTime(required=False))
    all_roles = graphene.List(RoleType)
    all_chats = graphene.List(ChatSessionType)
    all_messages = graphene.List(ChatMessageType, chat_id=graphene.ID(required=True))

    @login_required
    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')

        return user

    @login_required
    def resolve_all_customers(self, info, **kwargs):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'CustomerAll' in permissions_list or 'CustomerReadOnly' in permissions_list:
            return Customer.objects.filter(related_place=user.related_place)

        if 'CustomerName' in permissions_list and 'CustomerRole' not in permissions_list:
            return Customer.objects.filter(related_place=user.related_place).values('id', 'name')

    @login_required
    def resolve_all_customers_name(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if 'CustomerName' in permissions_list and 'CustomerAll' in permissions_list:
            return Customer.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_places(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "PlaceAll" in permissions_list or "PlaceReadOnly" in permissions_list:
            return Place.objects.filter(id=user.related_place.id)

    @login_required
    def resolve_all_saloons(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "SaloonAll" in permissions_list or "SaloonReadOnly" in permissions_list:
            return Saloon.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_tables(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "TableAll" in permissions_list or "TableReadOnly" in permissions_list:
            return Table.objects.filter(related_saloon__related_place=user.related_place)

    @login_required
    def resolve_all_inventory_ingredients(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "InventoryAll" in permissions_list or "InventoryReadOnly" in permissions_list:
            return InventoryIngredients.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_inventory_entry(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []

        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "InventoryAll" in permissions_list or "InventoryReadOnly" in permissions_list:
            query = InventoryEntry.objects.filter(related_place=user.related_place)
            if kwargs.get('time_start') and kwargs.get('time_end'):
                query = query.filter(date_time__gte=kwargs.get('time_start'), date_time__lte=kwargs.get('time_end'))
            if kwargs.get('ingredient_id'):
                filtered = query.filter(ingredient__id=kwargs.get('ingredient_id'))
            return query

    @login_required
    def resolve_all_inventory_out(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []

        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "InventoryAll" in permissions_list or "InventoryReadOnly" in permissions_list:
            query = InventoryOut.objects.filter(related_place=user.related_place)
            if kwargs.get('time_start') and kwargs.get('time_end'):
                query = query.filter(date_time__gte=kwargs.get('time_start'), date_time__lte=kwargs.get('time_end'))
            if kwargs.get('ingredient_id'):
                filtered = query.filter(ingredient__id=kwargs.get('ingredient_id'))
            return query

    @login_required
    def resolve_all_invoice(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "InvoiceAll" in permissions_list or "InvoiceReadOnly" in permissions_list or "InvoicePayment" in permissions_list:
            return Invoice.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_food(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "FoodAll" in permissions_list or "FoodReadOnly" in permissions_list:
            return Food.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_food_category(self, info):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "FoodAll" in permissions_list or "FoodReadOnly" in permissions_list:
            return FoodCategory.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_ingredients(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "FoodAll" in permissions_list or "FoodReadOnly" in permissions_list:
            return Ingredients.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_ingredients_category(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "FoodAll" in permissions_list or "FoodReadOnly" in permissions_list:
            return IngredientsCategory.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_food_ingredients(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "FoodAll" in permissions_list or "FoodReadOnly" in permissions_list:
            return FoodIngredients.objects.filter(food__related_place=user.related_place)

    @login_required
    def resolve_all_order(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "OrderAll" in permissions_list or "OrderReadOnly" in permissions_list:
            return Order.objects.filter(invoice__related_place=user.related_place)

    @login_required
    def resolve_all_shifts(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "ShiftAll" in permissions_list or "ShiftReadOnly" in permissions_list:
            return Shift.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_personnel_shift(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "ShiftPersonnelAll" in permissions_list or "ShiftPersonnelReadOnly" in permissions_list:
            return Shift.objects.filter(personnel__related_place=user.related_place)

    @login_required
    def resolve_all_personnel(self, info):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "PersonnelAll" in permissions_list or "PersonnelReadOnly" in permissions_list:
            return Personnel.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_roles(self, info):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "PersonnelAll" in permissions_list or "PersonnelReadOnly" in permissions_list:
            return Role.objects.filter(related_place=user.related_place)

    @login_required
    def resolve_all_food_without_category(self, info):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "FoodAll" in permissions_list or "FoodReadOnly" in permissions_list:
            return Food.objects.filter(related_place=user.related_place, category=None)

    @login_required
    def resolve_all_ingredients_without_category(self, info):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)

        if "FoodAll" in permissions_list or "FoodReadOnly" in permissions_list:
            return Ingredients.objects.filter(related_place=user.related_place, category=None)

    @login_required
    def resolve_all_chats(self, info):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permission = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permission:
                permissions_list.append(item.permission)

        if "ChatAll" in permissions_list:
            return ChatSession.objects.filter(related_place=user.related_place)
        else:
            final_list = []
            filter_chat = ChatSession.objects.filter(related_place=user.related_place)
            for item in filter_chat:
                if user in item.users.all():
                    final_list.append(item)

            return final_list

    @login_required
    def resolve_all_messages(self, info, chat_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permission = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permission:
                permissions_list.append(item.permission)

        if "ChatAll" in permissions_list:
            return ChatMessages.objects.filter(chat__id=chat_id)
        else:
            try:
                chat_inst = ChatSession.objects.get(id=chat_id)
                if user in chat_inst.users.all():
                    return ChatMessages.objects.filter(chat=chat_inst)
            except ChatSession.DoesNotExist:
                pass
