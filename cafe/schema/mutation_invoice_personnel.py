import graphene
from cafe.models import InvoicePersonnel, Personnel, RolePersonnel, Permissions
from .types import InvoicePersonnelType
from graphql_jwt.decorators import login_required


class InvoicePersonnelAddMutation(graphene.Mutation):

    class Arguments:
        personnel_id = graphene.ID()

    invoice = graphene.Field(InvoicePersonnelType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, personnel_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list or 'InvoiceAdd' in permissions_list:
            invoice_inst = InvoicePersonnel()
            try:
                invoice_inst.customer = Personnel.objects.get(id=personnel_id)
            except Personnel.DoesNotExist:
                return InvoicePersonnelAddMutation(invoice=None, message='Personnel Does Not Exist', status=False)

            invoice_inst.save()
            return InvoicePersonnelAddMutation(invoice=invoice_inst, message='', status=True)


class InvoicePersonnelEditMutation(graphene.Mutation):

    class Arguments:
        invoice_id = graphene.ID()
        personnel_id = graphene.ID()

    invoice = graphene.Field(InvoicePersonnelType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, personnel_id, invoice_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list or 'InvoiceEdit' in permissions_list:
            try:
                invoice_inst = InvoicePersonnel.objects.get(id=invoice_id)
                try:
                    invoice_inst.customer = Personnel.objects.get(id=personnel_id)
                except Personnel.DoesNotExist:
                    return InvoicePersonnelEditMutation(invoice=None, message='Personnel Does Not Exist', status=False)

                invoice_inst.save()
                return InvoicePersonnelEditMutation(invoice=invoice_inst, message='', status=True)
            except InvoicePersonnel.DoesNotExist:
                return InvoicePersonnelEditMutation(invoice=None, message='Personnel Invoice Does Not Exist',
                                                    status=False)


class InvoicePersonnelDeleteMutation(graphene.Mutation):

    class Arguments:
        invoice_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, invoice_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list or 'InvoiceDelete' in permissions_list:
            try:
                invoice_inst = InvoicePersonnel.objects.get(id=invoice_id)
                invoice_inst.delete()
                return InvoicePersonnelDeleteMutation(status=True)
            except InvoicePersonnel.DoesNotExist:
                return InvoicePersonnelDeleteMutation(status=False)


