import graphene
from cafe.models import InventoryIngredients, Ingredients, RolePersonnel, Permissions
from .types import InventoryIngredientsType
from graphql_jwt.decorators import login_required


class InventoryIngredientsAddMutation(graphene.Mutation):

    class Arguments:
        ingredients_id = graphene.ID()
        amount = graphene.Float()

    inventory = graphene.Field(InventoryIngredientsType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, ingredients_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InventoryAll' in permissions_list or 'InventoryAdd' in permissions_list:
            inventory_inst = InventoryIngredients()
            inventory_inst.related_place = user.related_place
            inventory_inst.amount = kwargs.get('amount')
            try:
                inventory_inst.ingredient = Ingredients.objects.get(id=ingredients_id)
                inventory_inst.save()
                return InventoryIngredientsAddMutation(inventory=inventory_inst, message='', status=True)
            except Ingredients.DoesNotExist:
                return InventoryIngredientsAddMutation(inventory=None,
                                                       message='Ingredient Does Not Exist',
                                                       status=False)


class InventoryIngredientsEditMutation(graphene.Mutation):

    class Arguments:
        inventory_id = graphene.ID()
        ingredients_id = graphene.ID()
        amount = graphene.Float()

    inventory = graphene.Field(InventoryIngredientsType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, inventory_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InventoryAll' in permissions_list or 'InventoryAdd' in permissions_list:
            try:
                inventory_inst = InventoryIngredients.objects.get(id=inventory_id)
                if kwargs.get('amount'):
                    inventory_inst.amount = kwargs.get('amount')
                if kwargs.get('ingredients_id'):
                    try:
                        inventory_inst.ingredient = Ingredients.objects.get(id=kwargs.get('ingredients_id'))
                    except Ingredients.DoesNotExist:
                        return InventoryIngredientsAddMutation(inventory=None,
                                                               message='Ingredient Does Not Exist',
                                                               status=False)
                inventory_inst.save()
                return InventoryIngredientsAddMutation(inventory=inventory_inst,
                                                       message='',
                                                       status=True)
            except InventoryIngredients.DoesNotExist:
                return InventoryIngredientsAddMutation(inventory=None,
                                                       message='Inventory Does Not Exist',
                                                       status=False)


class InventoryIngredientsDeleteMutation(graphene.Mutation):

    class Arguments:
        inventory_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, inventory_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InventoryAll' in permissions_list or 'InventoryDelete' in permissions_list:
            try:
                inventory_inst = InventoryIngredients.objects.get(id=inventory_id)
                inventory_inst.delete()
                return InventoryIngredientsDeleteMutation(status=True)
            except InventoryIngredients.DoesNotExist:
                return InventoryIngredientsDeleteMutation(status=False)

