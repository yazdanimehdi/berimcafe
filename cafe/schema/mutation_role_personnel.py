import graphene
from cafe.models import Role, Personnel,  RolePersonnel, Permissions
from .types import RolePersonnelType
from graphql_jwt.decorators import login_required


class RolePersonnelAddMutation(graphene.Mutation):

    class Arguments:
        role_id = graphene.ID()
        personnel_id = graphene.ID()

    role_personnel = graphene.Field(RolePersonnelType)
    message = graphene.String()
    status = graphene.Boolean()
    @login_required
    def mutate(self, info, role_id, personnel_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelAdd' in permissions_list:
            role_personnel_inst = RolePersonnel()
            try:
                role_personnel_inst.role = Role.objects.get(id=role_id)
            except Role.DoesNotExist:
                return RolePersonnelAddMutation(role_personnel=None, message='Role Does Not Exist', status=False)

            try:
                role_personnel_inst.personnel = Personnel.objects.get(id=personnel_id)
            except Personnel.DoesNotExist:
                return RolePersonnelAddMutation(role_personnel=None, message='Personnel Does Not Exist', status=False)

            role_personnel_inst.save()
            return RolePersonnelAddMutation(role_personnel=role_personnel_inst, message="", status=True)


class RolePersonnelEditMutation(graphene.Mutation):

    class Arguments:
        role_personnel_id = graphene.ID()
        role_id = graphene.ID()
        personnel_id = graphene.ID()

    role_personnel = graphene.Field(RolePersonnelType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, role_personnel_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelEdit' in permissions_list:
            try:
                role_personnel_inst = RolePersonnel.objects.get(id=role_personnel_id)
                if kwargs.get('role_id'):
                    try:
                        role_personnel_inst.role = Role.objects.get(id=kwargs.get('role_id'))
                    except Role.DoesNotExist:
                        return RolePersonnelEditMutation(role_personnel=role_personnel_inst,
                                                         message='Role Does Not Exist',
                                                         status=False)
                if kwargs.get('personnel_id'):
                    try:
                        role_personnel_inst.personnel = Personnel.objects.get(id=kwargs.get('personnel_id'))
                    except Personnel.DoesNotExist:
                        return RolePersonnelEditMutation(role_personnel=role_personnel_inst,
                                                         message='Personnel Does Not Exist',
                                                         status=False)
                role_personnel_inst.save()
                return RolePersonnelEditMutation(role_personnel=role_personnel_inst, message='', status=True)
            except RolePersonnel.DoesNotExist:
                return RolePersonnelEditMutation(role_personnel=None, message='Role Personnel Does Not Exist',
                                                 status=False)


class RolePersonnelDeleteMutation(graphene.Mutation):

    class Arguments:
        role_personnel_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, role_personnel_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelDelete' in permissions_list:
            try:
                role_personnel_inst = RolePersonnel.objects.get(id=role_personnel_id)
                role_personnel_inst.delete()
                return RolePersonnelDeleteMutation(status=True)
            except RolePersonnel.DoesNotExist:
                return RolePersonnelDeleteMutation(status=False)