import graphene
from cafe.models import RolePersonnel, Permissions
from .types import PermissionType
from graphql_jwt.decorators import login_required


class PermissionsAddMutation(graphene.Mutation):

    class Argument:
        choice = graphene.String()

    permission = graphene.Field(PermissionType)

    @login_required
    def mutate(self, info, choice):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelAdd' in permissions_list:
            permission_inst = Permissions()
            permission_inst.permission = choice
            permission_inst.save()
            return PermissionsAddMutation(permission=permission_inst)


class PermissionsEditMutation(graphene.Mutation):

    class Argument:
        permission_id = graphene.ID()
        choice = graphene.String()

    permission = graphene.Field(PermissionType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, permission_id, choice):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelEdit' in permissions_list:
            try:
                permission_inst = Permissions.objects.get(id=permission_id)
                permission_inst.permission = choice
                permission_inst.save()
                return PermissionsEditMutation(permission=permission_inst, message='', status=True)
            except Permissions.DoesNotExist:
                return PermissionsEditMutation(permission=None, message='Permission Does Not Exist', status=False)


class PermissionsDeleteMutation(graphene.Mutation):

    class Argument:
        permission_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, permission_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelDelete' in permissions_list:
            try:
                permission_inst = Permissions.objects.get(id=permission_id)
                permission_inst.Delete()
                return PermissionsDeleteMutation(status=True)
            except Permissions.DoesNotExist:
                return PermissionsDeleteMutation(status=False)