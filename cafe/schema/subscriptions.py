import graphene
from graphql_jwt.shortcuts import get_user_by_token

from cafe.models import ChatMessages, RolePersonnel, Permissions, ChatSession
from graphene_subscriptions.events import CREATED, UPDATED
from .types import ChatMessageType


class Subscription(graphene.ObjectType):
    chat_messages_created_updated = graphene.Field(ChatMessageType,
                                                   token=graphene.String(required=True))

    def resolve_chat_messages_created_updated(root, _, token):
        try:
            user = get_user_by_token(token)
            roles = RolePersonnel.objects.filter(personnel=user)
            permissions_list = []
            for role in roles:
                permissions = Permissions.objects.filter(permissionsrole__role=role.role)
                for item in permissions:
                    permissions_list.append(item.permission)
            if "ChatAll" in permissions_list:
                return root.filter(
                    lambda event:
                    (event.operation == CREATED or
                     event.operation == UPDATED) and
                    isinstance(event.instance, ChatMessages) and
                    event.instance.chat.related_place == user.related_place
                ).map(lambda event: event.instance)

            else:
                return root.filter(
                    lambda event:
                    (event.operation == CREATED or
                     event.operation == UPDATED) and
                    isinstance(event.instance, ChatMessages) and
                    user in event.instance.chat.users.all()
                ).map(lambda event: event.instance)
        except:
            pass
