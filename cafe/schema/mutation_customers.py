import graphene
from cafe.models import Customer, RolePersonnel, Permissions
from graphql_jwt.decorators import login_required
from .types import CustomerType


class CustomerCreateMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        phone1 = graphene.String(required=False)
        phone2 = graphene.String(required=False)
        email1 = graphene.String(required=False)
        email2 = graphene.String(required=False)
        address = graphene.String(required=False)
        extra_info = graphene.String(required=False)
        notes = graphene.String(required=False)

    customer = graphene.Field(CustomerType)

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'CustomerAll' in permissions_list or 'CustomerAdd' in permissions_list:
            customer = Customer()
            customer.name = kwargs.get('name')
            customer.address = kwargs.get('address')
            customer.phone1 = kwargs.get('phone1')
            customer.phone2 = kwargs.get('phone2')
            customer.email1 = kwargs.get('email1')
            customer.email2 = kwargs.get('email2')
            customer.extra_info = kwargs.get('extra_info')
            customer.notes = kwargs.get('notes')
            customer.related_place = user.related_place
            customer.save()
            return CustomerCreateMutation(customer=customer)


class CustomerEditMutation(graphene.Mutation):

    class Arguments:
        customer_id = graphene.Int(required=True)
        name = graphene.String()
        phone1 = graphene.String()
        phone2 = graphene.String()
        email1 = graphene.String()
        email2 = graphene.String()
        address = graphene.String()
        extra_info = graphene.String()
        notes = graphene.String()

    customer = graphene.Field(CustomerType)
    status = graphene.Boolean()
    message = graphene.String()

    @login_required
    def mutate(self, info, customer_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'CustomerAll' in permissions_list or 'CustomerEdit' in permissions_list:
            try:
                customer = Customer.objects.get(id=customer_id)
                if kwargs.get('name'):
                    customer.name = kwargs.get('name')
                if kwargs.get('address'):
                    customer.address = kwargs.get('address')
                if kwargs.get('phone1'):
                    customer.phone1 = kwargs.get('phone1')
                if kwargs.get('phone2'):
                    customer.phone2 = kwargs.get('phone2')
                if kwargs.get('email1'):
                    customer.email1 = kwargs.get('email1')
                if kwargs.get('email2'):
                    customer.email2 = kwargs.get('email2')
                if kwargs.get('extra_info'):
                    customer.extra_info = kwargs.get('extra_info')
                if kwargs.get('notes'):
                    customer.notes = kwargs.get('notes')
                customer.related_place = user.related_place
                customer.save()
                return CustomerEditMutation(customer=customer, message='', status=True)
            except Customer.DoesNotExist:
                return CustomerEditMutation(customer=None, message='Id Not Found', status=False)


class CustomerDeleteMutation(graphene.Mutation):

    class Arguments:
        customer_id = graphene.Int(required=True)

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, customer_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'CustomerAll' in permissions_list or 'CustomerDelete' in permissions_list:
            try:
                customer = Customer.objects.get(id=customer_id)
                customer.delete()
                return CustomerDeleteMutation(True)
            except Customer.DoesNotExist:
                return CustomerDeleteMutation(False)




