import graphene
from cafe.models import Role,  RolePersonnel, Permissions
from .types import RoleType
from graphql_jwt.decorators import login_required


class RoleAddMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()
        role_related = graphene.String()

    role = graphene.Field(RoleType)

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelAdd' in permissions_list:
            role_inst = Role()
            role_inst.name = kwargs.get('name')
            role_inst.role_related = kwargs.get('role_related')
            role_inst.related_place = user.related_place
            role_inst.save()
            return RoleAddMutation(role=role_inst)


class RoleEditMutation(graphene.Mutation):

    class Arguments:
        role_id = graphene.ID()
        name = graphene.String()
        role_related = graphene.String()

    role = graphene.Field(RoleType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, role_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelEdit' in permissions_list:
            try:
                role_inst = Role.objects.get(id=role_id)
                if kwargs.get('name'):
                    role_inst.name = kwargs.get('name')
                if kwargs.get('role_related'):
                    role_inst.role_related = kwargs.get('role_related')
                role_inst.save()
                return RoleEditMutation(role=role_inst, message='', status=True)
            except Role.DoesNotExist:
                return RoleEditMutation(role=None, message='Role Does Not Exist', status=True)


class RoleDeleteMutation(graphene.Mutation):

    class Arguments:
        role_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, role_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelDelete' in permissions_list:
            try:
                role_inst = Role.objects.get(id=role_id)
                role_inst.delete()
                return RoleDeleteMutation(status=True)
            except Role.DoesNotExist:
                return RoleDeleteMutation(status=False)