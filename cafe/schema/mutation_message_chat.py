import graphene
from django.core.files import File
from graphene_file_upload.scalars import Upload
from cafe.models import ChatMessages, ChatSession
from .types import ChatMessageType
from graphql_jwt.decorators import login_required


class ChatMessageAddMutation(graphene.Mutation):
    class Arguments:
        message = graphene.String()
        file = Upload()
        chat_session_id = graphene.ID(required=True)
        reply_message_id = graphene.ID()

    status = graphene.Boolean()
    message = graphene.String()
    messages = graphene.Field(ChatMessageType)

    @login_required
    def mutate(self, info, chat_session_id, **kwargs):
        user = info.context.user
        try:
            chat = ChatSession.objects.get(id=chat_session_id)
            if user in chat.users.all():
                message_inst = ChatMessages()
                message_inst.chat = chat
                message_inst.from_user = user
                if kwargs.get('message'):
                    message_inst.message = kwargs.get('message')
                if kwargs.get('file'):
                    message_inst.file = File(kwargs.get('file'))
                if kwargs.get('reply_message_id'):
                    try:
                        message_inst.reply_message = ChatMessages.objects.get('reply_message_id')
                    except ChatMessages.DoesNotExist:
                        pass
                message_inst.save()
                return ChatMessageAddMutation(messages=message_inst, message='', status=True)
            else:
                return ChatMessageAddMutation(messages=None, message='You Do Not Have Permission', status=False)
        except ChatSession.DoesNotExist:
            return ChatMessageAddMutation(messages=None, message='Chat Does Not Exist', status=False)


class ChatMessageSendReadMutation(graphene.Mutation):
    class Arguments:
        message_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, message_id):
        user = info.context.user
        try:
            message = ChatMessages.objects.get(id=message_id)
            if user in message.chat.users.all():
                message.read = True
                message.save()
                return ChatMessageSendReadMutation(status=True)
            else:
                return ChatMessageSendReadMutation(status=False)
        except ChatMessages.DoesNotExist:
            return ChatMessageSendReadMutation(status=False)
