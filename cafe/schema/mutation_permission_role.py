import graphene
from cafe.models import PermissionsRole,  RolePersonnel, Permissions, Role
from .types import PermissionsRoleType
from graphql_jwt.decorators import login_required


class PermissionRoleAddMutation(graphene.Mutation):

    class Arguments:
        role_id = graphene.ID()
        permission_id = graphene.ID()

    permission_role = graphene.Field(PermissionsRoleType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, role_id, permission_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelAdd' in permissions_list:
            permission_role_inst = PermissionsRole()
            try:
                permission_role_inst.permission = Permissions.objects.get(id=permission_id)
            except Permissions.DoesNotExist:
                return PermissionRoleAddMutation(permission_role=None, message='Permission Does Not Exist',
                                                 status=False)

            try:
                permission_role_inst.role = Role.objects.get(id=role_id)
            except Role.DoesNotExist:
                return PermissionRoleAddMutation(permission_role=None, message='Role Does Not Exist',
                                                 status=False)
            permission_role_inst.save()
            return PermissionRoleAddMutation(permission_role=permission_role_inst, message='', status=True)


class PermissionRoleEditMutation(graphene.Mutation):

    class Arguments:
        permission_role_id = graphene.ID()
        role_id = graphene.ID()
        permission_id = graphene.ID()

    permission_role = graphene.Field(PermissionsRoleType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, permission_role_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelEdit' in permissions_list:
            try:
                permission_role_inst = PermissionsRole.objects.get(id=permission_role_id)
                if kwargs.get('permission_id'):
                    try:
                        permission_role_inst.permission = Permissions.objects.get(id=kwargs.get('permission_id'))
                    except Permissions.DoesNotExist:
                        return PermissionRoleEditMutation(permission_role=permission_role_inst,
                                                          message='Permission Does Not Exist',
                                                          status=False)
                if kwargs.get('role_id'):
                    try:
                        permission_role_inst.role = Role.objects.get(id=kwargs.get('role_id'))
                    except Role.DoesNotExist:
                        return PermissionRoleEditMutation(permission_role=permission_role_inst,
                                                          message='Role Does Not Exist',
                                                          status=False)
                permission_role_inst.save()
                return PermissionRoleEditMutation(permission_role=permission_role_inst, message='', status=True)
            except PermissionsRole.DoesNotExist:
                return PermissionRoleEditMutation(permission_role=None, message='PermissionRole Does Not Exist',
                                                  status=False)


class PermissionRoleDeleteMutation(graphene.Mutation):

    class Arguments:
        permission_role_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, permission_role_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelEdit' in permissions_list:
            try:
                permission_role_inst = PermissionsRole.objects.get(id=permission_role_id)
                permission_role_inst.delete()
                return PermissionRoleDeleteMutation(status=True)
            except PermissionsRole.DoesNotExist:
                return PermissionRoleDeleteMutation(status=False)