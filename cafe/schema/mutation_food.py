import graphene
from cafe.models import Food, FoodCategory, RolePersonnel, Permissions
from .types import FoodType
from graphql_jwt.decorators import login_required


class FoodAddMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()
        description = graphene.String()
        price = graphene.Float()
        category_id = graphene.ID()
        discount_percent = graphene.Int()
        discount = graphene.Int()
        food_status = graphene.Boolean()
        related_production_place = graphene.String()

    food = graphene.Field(FoodType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodAdd' in permissions_list:
            food_inst = Food()
            food_inst.name = kwargs.get('name')
            food_inst.related_place = user.related_place
            if kwargs.get('category_id'):
                try:
                    food_inst.category = FoodCategory.objects.get(id=kwargs.get('category_id'))
                except FoodCategory.DoesNotExist:
                    return FoodAddMutation(food=None, message='Category Does Not Exist', status=False)
            food_inst.status = kwargs.get('food_status')
            if kwargs.get('discount_percent'):
                food_inst.discount_percent = kwargs.get('discount_percent')
            if kwargs.get('discount'):
                food_inst.discount = kwargs.get('discount')
            food_inst.description = kwargs.get('description')
            food_inst.related_production_place = kwargs.get('related_production_place')
            food_inst.price = kwargs.get('price')
            food_inst.save()
            return FoodAddMutation(food=food_inst, message='', status=True)


class FoodEditMutation(graphene.Mutation):
    class Arguments:
        food_id = graphene.ID()
        name = graphene.String()
        description = graphene.String()
        price = graphene.Float()
        category_id = graphene.ID()
        discount_percent = graphene.Int()
        discount = graphene.Int()
        food_status = graphene.Boolean()
        related_production_place = graphene.String()

    food = graphene.Field(FoodType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, food_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodEdit' in permissions_list:
            try:
                food_inst = Food.objects.get(id=food_id)
                if kwargs.get('name'):
                    food_inst.name = kwargs.get('name')
                if kwargs.get('category_id'):
                    try:
                        food_inst.category = FoodCategory.objects.get(id=kwargs.get('category_id'))
                    except FoodCategory.DoesNotExist:
                        return FoodEditMutation(food=food_inst, message='Category Does Not Exist', status=False)
                if kwargs.get('food_status'):
                    food_inst.status = kwargs.get('food_status')
                if kwargs.get('discount_percent'):
                    food_inst.discount_percent = kwargs.get('discount_percent')
                if kwargs.get('discount'):
                    food_inst.discount = kwargs.get('discount')
                if kwargs.get('description'):
                    food_inst.description = kwargs.get('description')
                if kwargs.get('related_production_place'):
                    food_inst.related_production_place = kwargs.get('related_production_place')
                if kwargs.get('price'):
                    food_inst.price = kwargs.get('price')
                food_inst.save()
                return FoodEditMutation(food=food_inst, message='', status=True)

            except Food.DoesNotExist:
                FoodEditMutation(food=None, message='Food Does Not Exist', status=False)


class FoodDeleteMutation(graphene.Mutation):

    class Arguments:
        food_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, food_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodDelete' in permissions_list:
            try:
                food_inst = Food.objects.get(id=food_id)
                food_inst.delete()
                return FoodDeleteMutation(status=True)
            except Food.DoesNotExist:
                return FoodDeleteMutation(status=False)

