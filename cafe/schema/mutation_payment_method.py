import graphene
from cafe.models import PaymentMethod, RolePersonnel, Permissions
from .types import PaymentMethodType
from graphql_jwt.decorators import login_required


class PaymentMethodAddMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()

    payment_method = graphene.Field(PaymentMethodType)

    @login_required
    def mutate(self, info, name):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list:
            payment_method_inst = PaymentMethod()
            payment_method_inst.name = name
            payment_method_inst.related_place = user.related_place
            payment_method_inst.save()
            return PaymentMethodAddMutation(payment_method=payment_method_inst)


class PaymentMethodEditMutation(graphene.Mutation):

    class Arguments:
        payment_method_id = graphene.ID()
        name = graphene.String()

    payment_method = graphene.Field(PaymentMethodType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, payment_method_id, name):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list:
            try:
                payment_method_inst = PaymentMethod.objects.get(id=payment_method_id)
                payment_method_inst.name = name
                payment_method_inst.save()
                return PaymentMethodEditMutation(payment_method=payment_method_inst, message='', status=True)
            except PaymentMethod.DoesNotExist:
                return PaymentMethodEditMutation(payment_method=None, message='Payment Method Does Not Exist',
                                                 status=False)


class PaymentMethodDeleteMutation(graphene.Mutation):

    class Arguments:
        payment_method_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, payment_method_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list:
            try:
                payment_method_inst = PaymentMethod.objects.get(id=payment_method_id)
                payment_method_inst.delete()
                return PaymentMethodDeleteMutation(status=True)
            except PaymentMethod.DoesNotExist:
                return PaymentMethodDeleteMutation(status=False)