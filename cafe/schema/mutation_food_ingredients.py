import graphene
from cafe.models import Ingredients, Food, FoodIngredients, RolePersonnel, Permissions
from .types import FoodIngredientsType
from graphql_jwt.decorators import login_required


class FoodIngredientsAddMutation(graphene.Mutation):

    class Arguments:
        food_id = graphene.ID()
        ingredient_id = graphene.ID()
        amount = graphene.Float()

    food_ingredients = graphene.Field(FoodIngredientsType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, food_id, ingredient_id, amount):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodAdd' in permissions_list:
            food_ingredient_inst = FoodIngredients()
            try:
                food_ingredient_inst.food = Food.objects.get(id=food_id)
            except Food.DoesNotExist:
                return FoodIngredientsAddMutation(food_ingredients=None, message='Food Does Not Exist', status=False)

            try:
                food_ingredient_inst.ingredient = Ingredients.objects.get(id=ingredient_id)
            except Ingredients.DoesNotExist:
                return FoodIngredientsAddMutation(food_ingredients=None, message='Ingredient Does Not Exist',
                                                  status=False)

            food_ingredient_inst.amount = amount
            food_ingredient_inst.save()
            return FoodIngredientsAddMutation(food_ingredients=food_ingredient_inst, message='', status=True)


class FoodIngredientsEditMutation(graphene.Mutation):

    class Arguments:
        food_ingredient_id = graphene.ID()
        food_id = graphene.ID()
        ingredient_id = graphene.ID()
        amount = graphene.Float()

    food_ingredients = graphene.Field(FoodIngredientsType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, food_ingredient_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodEdit' in permissions_list:
            try:
                food_ingredient_inst = FoodIngredients.objects.get(id=food_ingredient_id)
                if kwargs.get('food_id'):
                    try:
                        food_ingredient_inst.food = Food.objects.get(id=kwargs.get('food_id'))
                    except Food.DoesNotExist:
                        return FoodIngredientsEditMutation(food_ingredients=food_ingredient_inst,
                                                           message='Food Does Not Exist',
                                                           status=False)
                if kwargs.get('ingredient_id'):
                    try:
                        food_ingredient_inst.ingredient = Ingredients.objects.get(id=kwargs.get('ingredient_id'))
                    except Ingredients.DoesNotExist:
                        return FoodIngredientsEditMutation(food_ingredients=food_ingredient_inst,
                                                           message='Ingredient Does Not Exist',
                                                           status=False)
                if kwargs.get('amount'):
                    food_ingredient_inst.amount = kwargs.get('amount')
                food_ingredient_inst.save()
                return FoodIngredientsEditMutation(food_ingredients=food_ingredient_inst, message='', status=True)
            except FoodIngredients.DoesNotExist:
                return FoodIngredientsEditMutation(food_ingredients=None, message='Ingredient Does Not Exist',
                                                   status=False)


class FoodIngredientsDeleteMutation(graphene.Mutation):

    class Arguments:
        food_ingredient_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, food_ingredient_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'FoodAll' in permissions_list or 'FoodEdit' in permissions_list or 'FoodDelete' in permissions_list:
            try:
                food_ingredient_inst = FoodIngredients.objects.get(id=food_ingredient_id)
                food_ingredient_inst.delete()
                return FoodIngredientsDeleteMutation(status=True)
            except FoodIngredients.DoesNotExist:
                return FoodIngredientsDeleteMutation(status=False)