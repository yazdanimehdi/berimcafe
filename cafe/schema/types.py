from graphene_django.types import DjangoObjectType
from cafe.models import *


class InventoryOutType(DjangoObjectType):
    class Meta:
        model = InventoryOut


class InventoryEntryType(DjangoObjectType):
    class Meta:
        model = InventoryEntry


class CustomerType(DjangoObjectType):
    class Meta:
        model = Customer


class CustomerNameType(DjangoObjectType):
    class Meta:
        model = Customer
        fields = ['id', 'name']


class PlaceType(DjangoObjectType):
    class Meta:
        model = Place


class SaloonType(DjangoObjectType):
    class Meta:
        model = Saloon


class TableType(DjangoObjectType):
    class Meta:
        model = Table


class InventoryIngredientsType(DjangoObjectType):
    class Meta:
        model = InventoryIngredients


class InvoiceType(DjangoObjectType):
    class Meta:
        model = Invoice


class FoodType(DjangoObjectType):
    class Meta:
        model = Food


class FoodCategoryType(DjangoObjectType):
    class Meta:
        model = FoodCategory


class IngredientsType(DjangoObjectType):
    class Meta:
        model = Ingredients


class IngredientsCategoryType(DjangoObjectType):
    class Meta:
        model = IngredientsCategory


class FoodIngredientsType(DjangoObjectType):
    class Meta:
        model = FoodIngredients


class OrderType(DjangoObjectType):
    class Meta:
        model = Order


class ShiftType(DjangoObjectType):
    class Meta:
        model = Shift


class PersonnelShiftType(DjangoObjectType):
    class Meta:
        model = PersonnelShift


class PersonnelType(DjangoObjectType):
    class Meta:
        model = Personnel


class PermissionType(DjangoObjectType):
    class Meta:
        model = Permissions


class RoleType(DjangoObjectType):
    class Meta:
        model = Role


class PermissionsRoleType(DjangoObjectType):
    class Meta:
        model = PermissionsRole


class RolePersonnelType(DjangoObjectType):
    class Meta:
        model = RolePersonnel


class PaymentMethodType(DjangoObjectType):
    class Meta:
        model = PaymentMethod


class InvoicePersonnelType(DjangoObjectType):
    class Meta:
        model = InvoicePersonnel


class ChatMessageType(DjangoObjectType):
    class Meta:
        model = ChatMessages


class ChatSessionType(DjangoObjectType):
    class Meta:
        model = ChatSession
