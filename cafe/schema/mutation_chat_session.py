import graphene
from graphql_jwt.decorators import login_required
from .types import ChatSessionType
from cafe.models import ChatSession, Personnel


class ChatSessionAddMutation(graphene.Mutation):

    class Arguments:
        users_id = graphene.List(graphene.ID)
        name = graphene.String()

    chat_session = graphene.Field(ChatSessionType)

    @login_required
    def mutate(self, info, users_id, **kwargs):
        user = info.context.user

        chat = ChatSession()
        if kwargs.get('name'):
            chat.name = kwargs.get('name')
        chat.save()
        chat.users.add(user)
        for item in users_id:
            try:
                users = Personnel.objects.get(id=item)
                chat.users.add(users)
            except Personnel.DoesNotExist:
                return ChatSessionAddMutation(chat_session=None)

        chat.related_place = user.related_place
        return ChatSessionAddMutation(chat_session=chat)
