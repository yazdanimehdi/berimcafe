import graphene
from cafe.models import Table, RolePersonnel, Permissions, Saloon
from .types import TableType
from graphql_jwt.decorators import login_required


class TableAddMutation(graphene.Mutation):

    class Arguments:
        name = graphene.String()
        related_saloon_id = graphene.ID()
        chair_number = graphene.Int()
        occupied = graphene.Boolean()

    table = graphene.Field(TableType)
    status = graphene.Boolean()
    message = graphene.String()

    @login_required
    def mutate(self, info, related_saloon_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'TableAll' in permissions_list or 'TableAdd' in permissions_list:
            table_inst = Table()
            try:
                table_inst.table_name = kwargs.get('name')
                table_inst.related_saloon = Saloon.objects.get(id=related_saloon_id)
                table_inst.chair_number = kwargs.get('chair_number')
                table_inst.occupied = False
                table_inst.save()
                return TableAddMutation(table=table_inst, status=True, message="")
            except Saloon.DoesNotExist:
                return TableAddMutation(table=None, status=False, message="Saloon Does Not Exist")


class TableEditMutation(graphene.Mutation):

    class Arguments:
        table_id = graphene.ID()
        related_saloon_id = graphene.ID()
        chair_number = graphene.Int()
        occupied = graphene.Boolean()

    table = graphene.Field(TableType)
    status = graphene.Boolean()
    message = graphene.String()

    @login_required
    def mutate(self, info, table_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'TableAll' in permissions_list or 'TableEdit' in permissions_list:
            try:
                table_inst = Table.objects.get(id=table_id)
                if kwargs.get('name'):
                    table_inst.table_name = kwargs.get('name')

                if kwargs.get('chair_number'):
                    table_inst.chair_number = kwargs.get('chair_number')

                if kwargs.get('related_saloon_id'):
                    try:
                        table_inst.related_saloon = Saloon.objects.get(id=kwargs.get('related_saloon_id'))
                    except Saloon.DoesNotExist:
                        return TableEditMutation(table=None, status=False, message='Saloon Does Not Exist')

                if kwargs.get('occupied'):
                    table_inst.occupied = kwargs.get('occupied')

                table_inst.save()

            except Table.DoesNotExist:
                return TableEditMutation(table=None, status=False, message='Table Does Not Exist')


class TableDeleteMutation(graphene.Mutation):

    class Arguments:
        table_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, table_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'TableAll' in permissions_list or 'TableDelete' in permissions_list:
            try:
                table_inst = Table.objects.get(id=table_id)
                table_inst.delete()
                return TableDeleteMutation(status=True)
            except Table.DoesNotExist:
                return TableDeleteMutation(status=False)

