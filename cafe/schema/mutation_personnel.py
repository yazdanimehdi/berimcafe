import graphene
from cafe.models import Personnel, RolePersonnel, Permissions
from .types import PersonnelType
from graphql_jwt.decorators import login_required


class PersonnelAddMutation(graphene.Mutation):

    class Arguments:
        first_name = graphene.String()
        last_name = graphene.String()
        username = graphene.String()
        password = graphene.String()
        email = graphene.String()
        phone = graphene.Int()
        gender = graphene.String()

    personnel = graphene.Field(PersonnelType)

    @login_required
    def mutate(self, info, first_name, last_name, username, password, email, phone, gender):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelAdd' in permissions_list:
            personnel_number = Personnel.objects.filter(related_place=user.related_place).count()
            if personnel_number < user.related_place.personnel_add:
                personnel_inst = Personnel()
                personnel_inst.related_place = user.related_place
                personnel_inst.first_name = first_name
                personnel_inst.last_name = last_name
                personnel_inst.gender = gender
                personnel_inst.phone = phone
                personnel_inst.username = username
                personnel_inst.set_password(password)
                personnel_inst.email = email
                personnel_inst.save()
                return PersonnelAddMutation(personnel=personnel_inst)


class PersonnelEditMutation(graphene.Mutation):

    class Arguments:
        personnel_id = graphene.ID()
        first_name = graphene.String()
        last_name = graphene.String()
        username = graphene.String()
        password = graphene.String()
        email = graphene.String()
        phone = graphene.Int()
        gender = graphene.String()

    personnel = graphene.Field(PersonnelType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, personnel_id, **kwargs):

        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelEdit' in permissions_list:
            try:
                personnel_inst = Personnel.objects.get(id=personnel_id)

                if kwargs.get('first_name'):
                    personnel_inst.first_name = kwargs.get('first_name')

                if kwargs.get('last_name'):
                    personnel_inst.last_name = kwargs.get('last_name')

                if kwargs.get('password'):
                    personnel_inst.set_password(kwargs.get('password'))

                if kwargs.get('username'):
                    personnel_inst.username = kwargs.get('username')

                if kwargs.get('email'):
                    personnel_inst.email = kwargs.get('email')

                if kwargs.get('phone'):
                    personnel_inst.phone = kwargs.get('phone')

                if kwargs.get('gender'):
                    personnel_inst.gender = kwargs.get('gender')

                personnel_inst.save()

                return PersonnelEditMutation(personnel=personnel_inst, message='', status=True)
            except Personnel.DoesNotExist:
                return PersonnelEditMutation(personnel=None, message='Personnel Does Not Exist', status=False)


class PersonnelDeleteMutation(graphene.Mutation):

    class Arguments:
        personnel_id = graphene.String()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, personnel_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'PersonnelAll' in permissions_list or 'PersonnelDelete' in permissions_list:
            try:
                personnel_inst = Personnel.objects.get(id=personnel_id)
                personnel_inst.delete()
                return PersonnelDeleteMutation(status=True)
            except Personnel.DoesNotExist:
                return PersonnelDeleteMutation(status=False)

