import graphene
from cafe.models import Invoice, Customer, Table, PaymentMethod, RolePersonnel, Permissions
from .types import InvoiceType
from graphql_jwt.decorators import login_required


class InvoiceAddMutation(graphene.Mutation):

    class Arguments:
        table_id = graphene.ID()
        customer_id = graphene.ID()
        number = graphene.Int()
        discount_percent = graphene.Int()
        discount = graphene.Int()
        status = graphene.String()
        payment_method = graphene.ID()

    invoice = graphene.Field(InvoiceType)
    message = graphene.String()
    return_status = graphene.Boolean()

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list or 'InvoiceAdd' in permissions_list:
            invoice_inst = Invoice()
            invoice_inst.related_place = user.related_place
            if kwargs.get('customer_id'):
                try:
                    invoice_inst.customer = Customer.objects.get(id=kwargs.get('customer_id'))
                except Customer.DoesNotExist:
                    return InvoiceAddMutation(invoice=None, message='Customer Does Not Exist', return_status=False)
            if kwargs.get('table_id'):
                try:
                    invoice_inst.table = Table.objects.get(id=kwargs.get('table_id'))
                except Table.DoesNotExist:
                    return InvoiceAddMutation(invoice=None, message='Table Does Not Exist', return_status=False)
            invoice_inst.waiter = user
            invoice_inst.number = kwargs.get('number')

            if kwargs.get('discount_percent'):
                invoice_inst.discount_percent = kwargs.get('discount_percent')

            if kwargs.get('discount'):
                invoice_inst.discount_percent = kwargs.get('discount')

            invoice_inst.status = kwargs.get('status')

            if kwargs.get('payment_method'):
                try:
                    invoice_inst.payment_method = PaymentMethod.objects.get(id=kwargs.get('payment_method'))
                except PaymentMethod.DoesNotExist:
                    return InvoiceAddMutation(invoice=None, message='Payment Method Does Not Exist',
                                              return_status=False)
            invoice_inst.save()
            return InvoiceAddMutation(invoice=invoice_inst, message='', return_status=True)


class InvoiceEditMutation(graphene.Mutation):

    class Arguments:
        invoice_id = graphene.ID()
        table_id = graphene.ID()
        customer_id = graphene.ID()
        waiter_id = graphene.ID()
        number = graphene.Int()
        discount_percent = graphene.Int()
        discount = graphene.Int()
        status = graphene.String()
        payment_method = graphene.ID()

    invoice = graphene.Field(InvoiceType)
    message = graphene.String()
    return_status = graphene.Boolean()

    @login_required
    def mutate(self, info, invoice_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list or 'InvoiceAdd' in permissions_list:
            try:
                invoice_inst = Invoice.objects.get(id=invoice_id)
                if kwargs.get('customer_id'):
                    try:
                        invoice_inst.customer = Customer.objects.get(id=kwargs.get('customer_id'))
                    except Customer.DoesNotExist:
                        return InvoiceEditMutation(invoice=invoice_inst, message='Customer Does Not Exist', return_status=False)
                if kwargs.get('table_id'):
                    try:
                        invoice_inst.table = Table.objects.get(id=kwargs.get('table_id'))
                    except Table.DoesNotExist:
                        return InvoiceEditMutation(invoice=invoice_inst, message='Table Does Not Exist', return_status=False)

                if kwargs.get('number'):
                    invoice_inst.number = kwargs.get('number')

                if kwargs.get('discount_percent'):
                    invoice_inst.discount_percent = kwargs.get('discount_percent')

                if kwargs.get('discount'):
                    invoice_inst.discount_percent = kwargs.get('discount')

                if kwargs.get('status'):
                    invoice_inst.status = kwargs.get('status')

                if kwargs.get('payment_method'):
                    try:
                        invoice_inst.payment_method = PaymentMethod.objects.get(id=kwargs.get('payment_method'))
                    except PaymentMethod.DoesNotExist:
                        return InvoiceEditMutation(invoice=invoice_inst, message='Payment Method Does Not Exist',
                                                   return_status=False)
                invoice_inst.save()
                return InvoiceAddMutation(invoice=invoice_inst, message='', return_status=True)
            except Invoice.DoesNotExist:
                InvoiceEditMutation(invoice=None, message='Invoice Does Not Exist', return_status=False)


class InvoiceDeleteMutation(graphene.Mutation):

    class Arguments:
        invoice_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, invoice_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InvoiceAll' in permissions_list or 'InvoiceDelete' in permissions_list:
            try:
                invoice_inst = Invoice.objects.get(id=invoice_id)
                invoice_inst.delete()
                return InvoiceDeleteMutation(status=True)
            except Invoice.DoesNotExist:
                return InvoiceDeleteMutation(status=False)

