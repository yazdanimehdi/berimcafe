from .mutation_customers import CustomerCreateMutation, CustomerEditMutation, CustomerDeleteMutation
from .mutation_place import PlaceAddMutation, PlaceDeleteMutation, PlaceEditMutation
from .mutation_saloon import SaloonAddMutation, SaloonEditMutation, SaloonDeleteMutation
from .mutation_table import TableAddMutation, TableEditMutation, TableDeleteMutation
from .mutation_inventory_ingredients import InventoryIngredientsAddMutation,\
    InventoryIngredientsEditMutation, InventoryIngredientsDeleteMutation
from .mutation_invoice import InvoiceAddMutation, InvoiceEditMutation, InvoiceDeleteMutation
from .mutation_food import FoodAddMutation, FoodEditMutation, FoodDeleteMutation
from .mutation_food_category import FoodCategoryAddMutation, FoodCategoryEditMutation, FoodCategoryDeleteMutation
from .mutation_ingredients import IngredientsAddMutation, IngredientsEditMutation, IngredientsDeleteMutation
from .mutation_ingredient_category import IngredientsCategoryAddMutation, IngredientsCategoryEditMutation,\
    IngredientsCategoryDeleteMutation
from .mutation_food_ingredients import FoodIngredientsAddMutation, FoodIngredientsEditMutation,\
    FoodIngredientsDeleteMutation
from .mutation_order import OrderAddMutation, OrderEditMutation, OrderDeleteMutation
from .mutation_shift import ShiftAddMutation, ShiftEditMutation, ShiftDeleteMutation
from .mutation_personnel_shift import PersonnelShiftAddMutation, PersonnelShiftEditMutation,\
    PersonnelShiftDeleteMutation
from .mutation_personnel import PersonnelAddMutation, PersonnelEditMutation, PersonnelDeleteMutation
from .mutation_invoice_personnel import InvoicePersonnelAddMutation, InvoicePersonnelEditMutation,\
    InvoicePersonnelDeleteMutation
from .mutation_payment_method import PaymentMethodAddMutation, PaymentMethodEditMutation, PaymentMethodDeleteMutation
from .mutation_permissions import PermissionsAddMutation, PermissionsEditMutation, PermissionsDeleteMutation
from .mutation_role import RoleAddMutation, RoleEditMutation, RoleDeleteMutation
from .mutation_permission_role import PermissionRoleAddMutation, PermissionRoleEditMutation,\
    PermissionRoleDeleteMutation
from .mutation_role_personnel import RolePersonnelAddMutation, RolePersonnelEditMutation, RolePersonnelDeleteMutation
from .mutation_inventory_entry import InventoryEntryAddMutation, InventoryEntryDeleteMutation
from .mutation_inventory_out import InventoryOutAddMutation, InventoryOutDeleteMutation
from .mutation_chat_session import ChatSessionAddMutation
from .mutation_message_chat import ChatMessageSendReadMutation, ChatMessageAddMutation

class Mutation:
    create_customer = CustomerCreateMutation.Field()
    edit_customer = CustomerEditMutation.Field()
    delete_customer = CustomerDeleteMutation.Field()
    create_place = PlaceAddMutation.Field()
    edit_place = PlaceEditMutation.Field()
    delete_place = PlaceDeleteMutation.Field()
    create_saloon = SaloonAddMutation.Field()
    edit_saloon = SaloonEditMutation.Field()
    delete_saloon = SaloonDeleteMutation.Field()
    create_table = TableAddMutation.Field()
    edit_table = TableEditMutation.Field()
    delete_table = TableDeleteMutation.Field()
    create_inventory_ingredients = InventoryIngredientsAddMutation.Field()
    edit_inventory_ingredients = InventoryIngredientsEditMutation.Field()
    delete_inventory_ingredients = InventoryIngredientsDeleteMutation.Field()
    create_invoice = InvoiceAddMutation.Field()
    edit_invoice = InvoiceEditMutation.Field()
    delete_invoice = InvoiceDeleteMutation.Field()
    create_food = FoodAddMutation.Field()
    edit_food = FoodEditMutation.Field()
    delete_food = FoodDeleteMutation.Field()
    create_food_category = FoodCategoryAddMutation.Field()
    edit_food_category = FoodCategoryEditMutation.Field()
    delete_food_category = FoodCategoryDeleteMutation.Field()
    create_ingredient = IngredientsAddMutation.Field()
    edit_ingredient = IngredientsEditMutation.Field()
    delete_ingredient = IngredientsDeleteMutation.Field()
    create_ingredient_category = IngredientsCategoryAddMutation.Field()
    edit_ingredient_category = IngredientsCategoryEditMutation.Field()
    delete_ingredient_category = IngredientsCategoryDeleteMutation.Field()
    create_food_ingredient = FoodIngredientsAddMutation.Field()
    edit_food_ingredient = FoodIngredientsEditMutation.Field()
    delete_food_ingredient = FoodIngredientsDeleteMutation.Field()
    create_order = OrderAddMutation.Field()
    edit_order = OrderEditMutation.Field()
    delete_order = OrderDeleteMutation.Field()
    create_shift = ShiftAddMutation.Field()
    edit_shift = ShiftEditMutation.Field()
    delete_shift = ShiftDeleteMutation.Field()
    create_personnel_shift = PersonnelShiftAddMutation.Field()
    edit_personnel_shift = PersonnelShiftEditMutation.Field()
    delete_personnel_shift = PersonnelShiftDeleteMutation.Field()
    create_personnel = PersonnelAddMutation.Field()
    edit_personnel = PersonnelEditMutation.Field()
    delete_personnel = PersonnelDeleteMutation.Field()
    create_invoice_personnel = InvoicePersonnelAddMutation.Field()
    edit_invoice_personnel = InvoicePersonnelEditMutation.Field()
    delete_invoice_personnel = InvoicePersonnelDeleteMutation.Field()
    create_payment_method = PaymentMethodAddMutation.Field()
    edit_payment_method = PaymentMethodEditMutation.Field()
    delete_payment_method = PaymentMethodDeleteMutation.Field()
    create_permission = PermissionsAddMutation.Field()
    edit_permission = PermissionsEditMutation.Field()
    delete_permission = PermissionsDeleteMutation.Field()
    add_role = RoleAddMutation.Field()
    edit_role = RoleEditMutation.Field()
    delete_role = RoleDeleteMutation.Field()
    add_role_permission = PermissionRoleAddMutation.Field()
    edit_role_permission = PermissionRoleEditMutation.Field()
    delete_role_permission = PermissionRoleDeleteMutation.Field()
    create_role_personnel = RolePersonnelAddMutation.Field()
    edit_role_personnel = RolePersonnelEditMutation.Field()
    delete_role_personnel = RolePersonnelDeleteMutation.Field()
    create_inventory_entry = InventoryEntryAddMutation.Field()
    delete_inventory_entry = InventoryEntryDeleteMutation.Field()
    create_inventory_out = InventoryOutAddMutation.Field()
    delete_inventory_out = InventoryOutDeleteMutation.Field()
    create_chat_session = ChatSessionAddMutation.Field()
    create_chat_message = ChatMessageAddMutation.Field()
    read_chat_message = ChatMessageSendReadMutation.Field()





