import graphene
from cafe.models import InventoryOut, RolePersonnel, Permissions, Ingredients
from .types import InventoryOutType
from graphql_jwt.decorators import login_required


class InventoryOutAddMutation(graphene.Mutation):
    class Arguments:
        ingredient_id = graphene.ID()
        description = graphene.String()
        amount = graphene.Float()
        date_time = graphene.DateTime()

    inventory_out = graphene.Field(InventoryOutType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InventoryAll' in permissions_list or 'InventoryAdd' in permissions_list:
            inventory_out_inst = InventoryOut()
            inventory_out_inst.related_place = user.related_place
            try:
                inventory_out_inst.ingredient = Ingredients.objects.get(id=int(kwargs.get('ingredient_id')))
            except Ingredients.DoesNotExist:
                return InventoryOutAddMutation(inventory_out=None, message='Ingredient Does Not Exist', status=False)

            if kwargs.get('description'):
                inventory_out_inst.description = kwargs.get('description')

            inventory_out_inst.amount = kwargs.get('amount')
            inventory_out_inst.date_time = kwargs.get('date_time')
            inventory_out_inst.save()
            return InventoryOutAddMutation(inventory_out=inventory_out_inst, message='', status=True)


class InventoryOutDeleteMutation(graphene.Mutation):

    class Arguments:
        inventory_out_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, inventory_out_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'InventoryAll' in permissions_list or 'InventoryDelete' in permissions_list:
            try:
                InventoryOut.objects.get(id=inventory_out_id).delete()
                return InventoryOutDeleteMutation(status=True)
            except InventoryOut.DoesNotExist:
                return InventoryOutDeleteMutation(status=False)

