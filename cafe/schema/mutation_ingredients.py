import graphene
from cafe.models import Ingredients, IngredientsCategory, RolePersonnel, Permissions
from .types import IngredientsType
from graphql_jwt.decorators import login_required


class IngredientsAddMutation(graphene.Mutation):
    class Arguments:
        name = graphene.String()
        description = graphene.String()
        price = graphene.Float()
        category_id = graphene.ID()
        scale = graphene.String()

    ingredient = graphene.Field(IngredientsType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'IngredientsAll' in permissions_list or 'IngredientsAdd' in permissions_list:
            ingredients_inst = Ingredients()
            ingredients_inst.name = kwargs.get('name')
            ingredients_inst.related_place = user.related_place
            if kwargs.get('category_id'):
                try:
                    ingredients_inst.category = IngredientsCategory.objects.get(id=int(kwargs.get('category_id')))
                except IngredientsCategory.DoesNotExist:
                    return IngredientsAddMutation(ingredient=None, message='Category Does Not Exist', status=False)
            if kwargs.get('description'):
                ingredients_inst.description = kwargs.get('description')

            ingredients_inst.price = kwargs.get('price')
            ingredients_inst.scale = kwargs.get('scale')
            ingredients_inst.save()
            return IngredientsAddMutation(ingredient=ingredients_inst, message='', status=True)


class IngredientsEditMutation(graphene.Mutation):
    class Arguments:
        ingredient_id = graphene.ID()
        name = graphene.String()
        description = graphene.String()
        price = graphene.Float()
        category_id = graphene.ID()
        scale = graphene.String()

    ingredient = graphene.Field(IngredientsType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, ingredient_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'IngredientsAll' in permissions_list or 'IngredientsEdit' in permissions_list:
            try:
                ingredients_inst = Ingredients.objects.get(id=ingredient_id)
                if kwargs.get('name'):
                    ingredients_inst.name = kwargs.get('name')

                if kwargs.get('category_id'):
                    try:
                        ingredients_inst.category = IngredientsCategory.objects.get(id=kwargs.get('category_id'))
                    except IngredientsCategory.DoesNotExist:
                        return IngredientsEditMutation(food=ingredients_inst, message='Category Does Not Exist', status=False)

                if kwargs.get('description'):
                    ingredients_inst.description = kwargs.get('description')

                if kwargs.get('price'):
                    ingredients_inst.price = kwargs.get('price')

                if kwargs.get('scale'):
                    ingredients_inst.scale = kwargs.get('scale')
                ingredients_inst.save()
                return IngredientsEditMutation(ingredient=ingredients_inst, message='', status=True)
            except Ingredients.DoesNotExist:
                return IngredientsEditMutation(food=None, message='Ingredient Does Not Exist', status=False)


class IngredientsDeleteMutation(graphene.Mutation):
    class Arguments:
        ingredient_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, ingredient_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'IngredientsAll' in permissions_list or 'IngredientsDelete' in permissions_list:
            try:
                ingredient_inst = Ingredients.objects.get(id=ingredient_id)
                ingredient_inst.delete()
                return IngredientsDeleteMutation(status=True)
            except Ingredients.DoesNotExist:
                return IngredientsDeleteMutation(status=False)

