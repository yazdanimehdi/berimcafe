import graphene
from cafe.models import Invoice, Order, Food, RolePersonnel, Permissions
from .types import OrderType
from graphql_jwt.decorators import login_required
from django.utils import timezone


class OrderAddMutation(graphene.Mutation):

    class Arguments:
        food_id = graphene.ID()
        invoice_id = graphene.ID()
        note = graphene.String()
        amount = graphene.Int()
        discount = graphene.Int()
        discount_percent = graphene.Int()
        order_status = graphene.String()

    order = graphene.Field(OrderType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, food_id, invoice_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'OrderAll' in permissions_list or 'OrderAdd' in permissions_list:
            order_inst = Order()
            try:
                order_inst.food = Food.objects.get(id=food_id)
            except Food.DoesNotExist:
                return OrderAddMutation(order=None, message='Food Does Not Exist', status=False)

            try:
                order_inst.invoice = Invoice.objects.get(id=invoice_id)
            except Invoice.DoesNotExist:
                return OrderAddMutation(order=None, message='Invoice Does Not Exist', status=False)

            order_inst.amount = kwargs.get('amount')
            if kwargs.get('discount'):
                order_inst.discount = kwargs.get('discount')

            if kwargs.get('discount_percent'):
                order_inst.discount_percent = kwargs.get('discount_percent')

            if kwargs.get('note'):
                order_inst.note = kwargs.get('note')

            order_inst.status = kwargs.get('order_status')

            order_inst.save()
            return OrderAddMutation(order=order_inst, message='', status=True)


class OrderEditMutation(graphene.Mutation):

    class Arguments:
        order_id = graphene.ID()
        food_id = graphene.ID()
        invoice_id = graphene.ID()
        note = graphene.String()
        amount = graphene.Int()
        discount = graphene.Int()
        discount_percent = graphene.Int()
        order_status = graphene.String()

    order = graphene.Field(OrderType)
    message = graphene.String()
    status = graphene.Boolean()

    @login_required
    def mutate(self, info, order_id, **kwargs):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'OrderAll' in permissions_list or 'OrderEdit' in permissions_list:
            try:
                order_inst = Order.objects.get(id=order_id)
                if kwargs.get('food_id'):
                    try:
                        order_inst.food = Food.objects.get(id=kwargs.get('food_id'))
                    except Food.DoesNotExist:
                        return OrderEditMutation(order=order_inst, message='Food Does Not Exist', status=False)

                if kwargs.get('invoice_id'):
                    try:
                        order_inst.invoice = Invoice.objects.get(id=kwargs.get('invoice_id'))
                    except Invoice.DoesNotExist:
                        return OrderEditMutation(order=order_inst, message='Invoice Does Not Exist', status=False)

                if kwargs.get('discount'):
                    order_inst.discount = kwargs.get('discount')

                if kwargs.get('discount_percent'):
                    order_inst.discount_percent = kwargs.get('discount_percent')

                if kwargs.get('note'):
                    order_inst.note = kwargs.get('note')

                if kwargs.get('amount'):
                    order_inst.amount = kwargs.get('amount')

                if kwargs.get('order_status'):
                    order_inst.status = kwargs.get('order_status')
                    if order_inst.status == 'DO':
                        order_inst.date_and_time_delivered = timezone.datetime.now()

                order_inst.save()
                return OrderEditMutation(order=order_inst, message='', status=True)

            except Order.DoesNotExist:
                return OrderEditMutation(order=None, message='Order Does Not Exist', status=False)


class OrderDeleteMutation(graphene.Mutation):

    class Arguments:
        order_id = graphene.ID()

    status = graphene.Boolean()

    @login_required
    def mutate(self, info, order_id):
        user = info.context.user
        roles = RolePersonnel.objects.filter(personnel=user)
        permissions_list = []
        for role in roles:
            permissions = Permissions.objects.filter(permissionsrole__role=role.role)
            for item in permissions:
                permissions_list.append(item.permission)
        if 'OrderAll' in permissions_list or 'OrderDelete' in permissions_list:
            try:
                order_inst = Order.objects.get(id=order_id)
                order_inst.delete()
                return OrderDeleteMutation(status=True)
            except Order.DoesNotExist:
                return OrderDeleteMutation(status=False)