from django.contrib import admin
from cafe.models import *

# Register your models here.
admin.site.register(Personnel)
admin.site.register(Place)
admin.site.register(PermissionsRole)
admin.site.register(Role)
admin.site.register(RolePersonnel)
admin.site.register(Food)
admin.site.register(FoodIngredients)
admin.site.register(FoodCategory)
admin.site.register(Invoice)
admin.site.register(Order)
admin.site.register(Permissions)
admin.site.register(Customer)
admin.site.register(IngredientsCategory)
admin.site.register(Ingredients)
admin.site.register(InventoryIngredients)
admin.site.register(InventoryEntry)
admin.site.register(InventoryOut)
admin.site.register(ChatSession)
admin.site.register(ChatMessages)
